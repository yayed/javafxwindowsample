module InterGraph {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires org.testng;

    exports home to javafx.graphics;
    exports models to org.testng;
    opens controllers to javafx.fxml;
}

