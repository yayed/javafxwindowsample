//package org.o7planning.javafx.window;
//
//import java.io.*;
//import java.util.logging.*;
//import javafx.application.*;
//import javafx.event.*;
//import javafx.scene.*;
//import javafx.scene.control.*;
//import javafx.scene.layout.*;
//import javafx.stage.*;
//import java.lang.Object;
//import org.apache.poi.ss.usermodel.Cell;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.xssf.usermodel.XSSFSheet;
//import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//
///**
// *
// * @author Sedrick
// */
//public class Excel extends Application {
//
//    @Override
//    public void start(Stage primaryStage)
//    {
//        XSSFWorkbook workbook = new XSSFWorkbook();
//        XSSFSheet sheet = workbook.createSheet("Java Books");
//
//        //Data to write to Excel file.
//        Object[][] bookData = {
//                {"Head First Java", "Kathy Serria", 79},
//                {"Effective Java", "Joshua Bloch", 36},
//                {"Clean Code", "Robert martin", 42},
//                {"Thinking in Java", "Bruce Eckel", 35},};
//
//        int rowCount = 0;
//
//        for (Object[] aBook : bookData) {
//            XSSFRow row = sheet.createRow(++rowCount);
//
//            int columnCount = 0;
//
//            for (Object field : aBook) {
//                XSSFCell cell = row.createCell(++columnCount);
//                if (field instanceof String) {
//                    cell.setCellValue((String) field);
//                }
//                else if (field instanceof Integer) {
//                    cell.setCellValue((Integer) field);
//                }
//            }
//
//        }
//
//        Button btn = new Button();
//        btn.setText("Save File");
//        btn.setOnAction(new EventHandler<ActionEvent>() {
//
//            @Override
//            public void handle(ActionEvent event)
//            {
//                FileChooser fileChooser = new FileChooser();
//
//                //Set extension filter to .xlsx files
//                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xlsx)", "*.xlsx");
//                fileChooser.getExtensionFilters().add(extFilter);
//
//                //Show save file dialog
//                File file = fileChooser.showSaveDialog(primaryStage);
//
//                //If file is not null, write to file using output stream.
//                if (file != null) {
//                    try (FileOutputStream outputStream = new FileOutputStream(file.getAbsolutePath())) {
//                        workbook.write(outputStream);
//                    }
//                    catch (IOException ex) {
//                        Logger.getLogger(Excel.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//            }
//        });
//
//        StackPane root = new StackPane();
//        root.getChildren().add(btn);
//
//        Scene scene = new Scene(root, 300, 250);
//
//        primaryStage.setTitle("Hello World!");
//        primaryStage.setScene(scene);
//        primaryStage.show();
//    }
//
//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String[] args)
//    {
//        launch(args);
//    }
//
//}