package org.o7planning.javafx.window;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ThemeMaker extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Pane previewPane = createPreviewPane();
        Pane controlPane = createControlPane(previewPane);

        Pane layout = new VBox(
                30,
                controlPane,
                previewPane
        );
        layout.setPadding(new Insets(10));

        stage.setScene(new Scene(layout));
        stage.show();
    }

    private Pane createControlPane(Pane previewPane) {
        ColorPicker backgroundColorPicker = new ColorPicker(Color.web("#b3ccff"));
        ColorPicker textColorPicker = new ColorPicker(Color.web("#4d804d"));
        ColorPicker controlColorPicker = new ColorPicker(Color.web("#ffe6cc"));

        GridPane controlPane = new GridPane();
        controlPane.setHgap(5);
        controlPane.setVgap(5);
        controlPane.addRow(0, new Label("Background color:"), backgroundColorPicker);
        controlPane.addRow(1, new Label("Text color:"), textColorPicker);
        controlPane.addRow(2, new Label("Control color:"), controlColorPicker);

        backgroundColorPicker.valueProperty().addListener((observable, oldColor, newColor) ->
                setThemeColors(previewPane, backgroundColorPicker.getValue(), textColorPicker.getValue(), controlColorPicker.getValue())
        );
        textColorPicker.valueProperty().addListener((observable, oldColor, newColor) ->
                setThemeColors(previewPane, backgroundColorPicker.getValue(), textColorPicker.getValue(), controlColorPicker.getValue())
        );
        controlColorPicker.valueProperty().addListener((observable, oldColor, newColor) ->
                setThemeColors(previewPane, backgroundColorPicker.getValue(), textColorPicker.getValue(), controlColorPicker.getValue())
        );

        setThemeColors(previewPane, backgroundColorPicker.getValue(), textColorPicker.getValue(), controlColorPicker.getValue());

        return controlPane;
    }

    private void setThemeColors(Pane previewPane, Color backgroundColor, Color textColor, Color controlColor) {
        previewPane.setStyle(
                "-fx-background-color: " + toHexString(backgroundColor) + ";" +
                        "-fx-text-background-color: " + toHexString(textColor) + ";" +
                        "-fx-base: " + toHexString(controlColor) + ";"
        );
    }

    private Pane createPreviewPane() {
        Label label = new Label(
                "Exercice d'entrainement à l'utilisation de javaFX.");
        label.setWrapText(true);
        Button btn = new Button("Boutton Exemple    ");
        Pane previewPane = new VBox(10, label, btn);

        previewPane.setPadding(new Insets(5));
        previewPane.setPrefWidth(200);

        return previewPane;
    }

    // from https://stackoverflow.com/a/56733608/1155209 "How to get hex web String from JavaFX ColorPicker color?"
    private String toHexString(Color value) {
        return "#" + (format(value.getRed()) + format(value.getGreen()) + format(value.getBlue()) + format(value.getOpacity()))
                .toUpperCase();
    }

    private String format(double val) {
        String in = Integer.toHexString((int) Math.round(val * 255));
        return in.length() == 1 ? "0" + in : in;
    }

    public static void main(String[] args) {
        launch(args);
    }
}