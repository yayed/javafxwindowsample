package org.o7planning.javafx.window;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import java.sql.*;

public class window extends Application{


    public void start(final Stage primaryStage) {

        Button button = new Button();
        button.setText("Open a New Window");

        button.setOnAction(event -> {

            Label secondLabel = new Label("I'm a Label on new Window");
            Label age = new Label("Age:");
            Label nom = new Label("Nom  :");
            Label prenom = new Label("Prenom:");
            Label ville = new Label("Ville:");

            TextField textField = new TextField ();
            HBox hb = new HBox();
            hb.getChildren().addAll(secondLabel, textField);
            hb.setSpacing(10);
            StackPane secondaryLayout = new StackPane();

            secondaryLayout.getChildren().add(secondLabel);

            Scene secondScene = new Scene(secondaryLayout, 230, 100);

            // New window (Stage)
            Stage newWindow = new Stage();
            newWindow.setTitle("Second Stage");
            newWindow.setScene(secondScene);

            // Set position of second window, related to primary window.
            newWindow.setX(primaryStage.getX() + 200);
            newWindow.setY(primaryStage.getY() + 100);

            newWindow.show();

            try {
                Class.forName("com.mysql.jdbc.Driver");

                Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/test" ,"root","");
                Statement statement= connection.createStatement();

                String requete="INSERT INTO utilisateur(Age,nom,prénom,Ville) VALUES ('28','test','exec','montard')";
                statement.executeUpdate(requete);
            } catch (ClassNotFoundException | SQLException ex) {
                System.err.println(ex.getMessage());
            }
        });


        StackPane root = new StackPane();
        root.getChildren().add(button);

        Scene scene = new Scene(root, 450, 250);

        primaryStage.setTitle("JavaFX Open a new Window");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}