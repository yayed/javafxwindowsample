
package models;



import org.testng.AssertJUnit;
import org.testng.annotations.*;

public class UserTest {
    @Test
    public void createNewUser()
    {
        User test = new User();
        test.setEmail("test@test.fr");
        test.setDob("19/10/1998");
        test.setGender("M");
        test.setFirstname("Jack");
        test.setLastname("Reacher");
        System.out.println("email : "+ test.getEmail() + "Date of birth : "+ test.getDob() + "Gender : "+ test.getGender() + "firstname : "+ test.getFirstname() + "lastname : "+ test.getLastname());

        AssertJUnit.assertEquals(test.getGender(),"M");
    }
}
